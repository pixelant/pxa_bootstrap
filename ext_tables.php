<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

    # Add Static for pxa_bootstrap
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Pxa Bootstrap');
    
    # Add Static for a example page
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript/Examples', 'Pxa Bootstrap Example');

# *****************
# Default settings
# *****************
    # Set default content header to Layout 2
$TCA['tt_content']['columns']['header_layout']['config']['default'] = 2;
    
    # Set default "Indentation and Frames" to "No Frame"
$TCA['tt_content']['columns']['section_frame']['config']['default'] = 66;

# *****************
# New header_layout
# *****************
    ## Add additional header (page-header)
$TCA['tt_content']['columns']['header_layout']['config']['items']['8']['0'] = 'LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.header_layout.item.8';
$TCA['tt_content']['columns']['header_layout']['config']['items']['8']['1'] = 8;

# *****************
# Add Select Box "Responsive Utility Class" on tt_content.
# *****************
$tempColumns = array (
    'tx_pxabootstrap_responsive_utility_class' => array (        
        'exclude' => 0,        
        'label' => 'LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.tx_pxabootstrap_responsive_utility_class',        
        'config' => array (
            'type' => 'select',
            'items' => array (
                array('LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.tx_pxabootstrap_responsive_utility_class.I.0', ''),
                array('LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.tx_pxabootstrap_responsive_utility_class.I.1', 'visible-xs'),
                array('LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.tx_pxabootstrap_responsive_utility_class.I.2', 'visible-sm'),
                array('LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.tx_pxabootstrap_responsive_utility_class.I.3', 'visible-md'),
                array('LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.tx_pxabootstrap_responsive_utility_class.I.4', 'hidden-xs'),
                array('LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.tx_pxabootstrap_responsive_utility_class.I.5', 'hidden-sm'),
                array('LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.tx_pxabootstrap_responsive_utility_class.I.6', 'hidden-md'),
                array('LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.tx_pxabootstrap_responsive_utility_class.I.7', 'visible-lg'),
                array('LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.tx_pxabootstrap_responsive_utility_class.I.8', 'hidden-lg'),
            ),
            'size' => 1,    
            'maxitems' => 1,
        )
    ),
);

# *****************
# Load tt_content TCA so we can modify it and insert out new field
# *****************
t3lib_div::loadTCA('tt_content');
t3lib_extMgm::addTCAcolumns('tt_content',$tempColumns,1);
t3lib_extMgm::addToAllTCAtypes('tt_content','tx_pxabootstrap_responsive_utility_class;;;;1-1-1',"","after:linkToTop");


# *****************
# Add Image Effects - using image_frames column which is disabled by css-styled-content by default 
# *****************
    # Clear original items
unset($TCA['tt_content']['columns']['image_frames']['config']['items']);
    
    # Change the label
$TCA['tt_content']['columns']['image_frames']['label'] = 'LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.image_frames';
    
    # Add custom frames to items
$TCA['tt_content']['columns']['image_frames']['config']['items']['0']['0'] = 'LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.image_frames.I.0';
$TCA['tt_content']['columns']['image_frames']['config']['items']['0']['1'] = 0;
$TCA['tt_content']['columns']['image_frames']['config']['items']['1']['0'] = 'LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.image_frames.I.1';
$TCA['tt_content']['columns']['image_frames']['config']['items']['1']['1'] = 1;
$TCA['tt_content']['columns']['image_frames']['config']['items']['2']['0'] = 'LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.image_frames.I.2';
$TCA['tt_content']['columns']['image_frames']['config']['items']['2']['1'] = 2;
$TCA['tt_content']['columns']['image_frames']['config']['items']['3']['0'] = 'LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.image_frames.I.3';
$TCA['tt_content']['columns']['image_frames']['config']['items']['3']['1'] = 3;

# *****************
# Image Adjustments - remove items in the "Number of Columns" select-box (5,7,8)
# *****************
unset($TCA['tt_content']['columns']['imagecols']['config']['items']['4']);
unset($TCA['tt_content']['columns']['imagecols']['config']['items']['6']);
unset($TCA['tt_content']['columns']['imagecols']['config']['items']['7']);

# *****************
# Table adjustments - change flexform for pi_flexform when *,table
# *****************
$TCA['tt_content']['columns']['pi_flexform']['config']['ds']['*,table'] = 'FILE:EXT:pxa_bootstrap/Configuration/FlexForms/flexform_table_ds.xml';

# *****************
# Showitem adjustments - change showitem for type table, remove layout field.
# *****************
$TCA['tt_content']['types']['table']['showitem'] = 'CType;;4;;1-1-1, hidden, header;;3;;2-2-2, linkToTop;;;;4-4-4, tx_pxabootstrap_responsive_utility_class;;;;1-1-1,           --div--;LLL:EXT:cms/locallang_ttc.xlf:CType.I.5;;10;;3-3-3, cols, bodytext;;9;nowrap:wizards[table], pi_flexform,           --div--;LLL:EXT:cms/locallang_tca.xlf:pages.tabs.access, starttime, endtime, fe_group, , --div--;LLL:EXT:flux/Resources/Private/Language/locallang.xlf:tt_content.tabs.relation, tx_flux_parent, tx_flux_column, tx_flux_children;LLL:EXT:flux/Resources/Private/Language/locallang.xlf:tt_content.tx_flux_children';
?>