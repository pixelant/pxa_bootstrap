<?php

if (!defined ('TYPO3_MODE'))
	die('Access denied.');

	# Add page ts 
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:pxa_bootstrap/Configuration/TypoScript/pageTsConfig.ts">');

	# Hook render-preProcess (for less compiler)
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_pagerenderer.php']['render-preProcess']['pxa_bootstrap'] = 'EXT:pxa_bootstrap/Classes/Hooks/RenderPreProcessHook.php:&tx_Pxabootstrap_Hooks_RenderPreProcessHook->renderPreProcess';

	# Cache settings pxa_bootsrtap (less)
if (!is_array($TYPO3_CONF_VARS['SYS']['caching']['cacheConfigurations']['pxa_bootstrap'])) {
	$TYPO3_CONF_VARS['SYS']['caching']['cacheConfigurations']['pxa_bootstrap'] = array(
		'frontend' => 't3lib_cache_frontend_VariableFrontend',
		'backend' => 't3lib_cache_backend_FileBackend',
		'options' => array(
				'defaultLifetime' => 3600*24*7,
			),
	);
}

?>