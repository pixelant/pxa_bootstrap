<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2014 Pixelant AB <info@pixelant.se>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * Hook to create css files from less files
 * 
 * @author Mats Svensson <mats@pixelant.se>
 *
 */
class tx_Pxabootstrap_Hooks_RenderPreProcessHook {

	/**
	* @var string
	* 
	*/
	protected $lessContent;

	/**
	* @var array
	* 
	*/
	protected $errors;

	/**
	* @var array
	* 
	*/
	protected $lessFiles;


	/**
	* @var array
	* 
	*/
	protected $configuration;

	/**
	 * Function renderPreProcess
	 * 
	 * @param array $params Array of css/js/header/footer files
	 * @param object $pageRenderer PageRenderer object
	 * @return void
	 *
	 */
	public function renderPreProcess(&$params, $pageRenderer) {

		if (!defined ('TYPO3_MODE')) die('Access denied.');

		if (TYPO3_MODE == 'BE') return;

			// Extra check that class is loaded, sin some rare cases exception is thrown because it isn't autoloaded.
		if ( !class_exists('Less_Autoloader') ) {
			require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('pxa_bootstrap') . 'Resources/Public/Contrib/less.php/lib/Less/Autoloader.php');
		}

		$memUsage = memory_get_usage();
		$start = microtime(true);
		$functionStarted = mktime();
		$isDevelopIp = TYPO3\CMS\Core\Utility\GeneralUtility::cmpIP($_SERVER['REMOTE_ADDR'], $GLOBALS['TSFE']->TYPO3_CONF_VARS['SYS']['devIPmask']);
		$siteName = TYPO3\CMS\Core\Utility\File\BasicFileUtility::cleanFileName($GLOBALS['TSFE']->TYPO3_CONF_VARS['SYS']['sitename']);

			// Get typoscript
		$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
		$configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
		$fullTypoScript = $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);

			// Return if no less configuration in plugin
		if (!isset($fullTypoScript['plugin.']['tx_pxabootstrap.']['settings.']['less.'])) return;

			// get ts configuration to array and remove dots from array
		$this->configuration = TYPO3\CMS\Core\Utility\GeneralUtility::removeDotsFromTS($fullTypoScript['plugin.']['tx_pxabootstrap.']['settings.']['less.']);

			// Return if less compilation is disabled.
		if ($this->configuration['disabled'] == '1') {
				// Write debug output here if enabled
			if ( (integer)$this->configuration['debug'] > 0 && TYPO3_MODE == 'FE' && $isDevelopIp ) {
				\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump(array(
					'CONFIGURATION' => $this->configuration,
					'LESS COMPILE DISABLED' => $this->configuration['disabled'],
				));
			}
			return;
		}

			// Register Less Autoloader
		Less_Autoloader::register();

			// get output filename
		$cssOutputDir = 'typo3temp/pxa_bootstrap';
		$cssOutputFilename = (strlen($this->configuration['outputCssFilename']) > 0) ? $this->configuration['outputCssFilename'] : $siteName;
		$cssFilename = TYPO3\CMS\Core\Utility\File\BasicFileUtility::cleanFileName($cssOutputFilename.".css");
		$cssRelativeFilename = $cssOutputDir . "/" . $cssFilename;
		$cssAbsoluteFilename = PATH_site . $cssRelativeFilename;

			// Fetch content from less files ("raw")
		$this->lessContent = $this->loadLessFilesContent($cssAbsoluteFilename);

			// Check cache
		$cache = $GLOBALS['typo3CacheManager']->getCache('pxa_bootstrap');
		$cacheKey = hash('sha1',$cssRelativeFilename);
		$contentHash = hash('sha1',$this->lessContent);
		$contentHashCache = '';

			// Get previous cache hash
		if ($cache->has($cacheKey)) {
			$contentHashCache = $cache->get($cacheKey);
		}

			// If no cache hash or prevous cache hash doesn't match with current cache hash try to pares less file
		if ($contentHashCache == '' || $contentHashCache != $contentHash) {
			try {
					// Init Options
				$options = array();

				if ($this->configuration['compress'] == '1') {
					$options = array('compress' => true);
				}
				$options['relativeUrls'] = false;
					// Create Less_Parser Object
				$parser = new Less_Parser($options);

					// Try to parse file
				$parser->parse($this->lessContent, $GLOBALS['TSFE']->baseUrl);

					// get parsed css
				$this->lessContent = $parser->getCss();

					// Write parsed css to file
				$result = TYPO3\CMS\Core\Utility\GeneralUtility::writeFileToTypo3tempDir($cssAbsoluteFilename, $this->lessContent);

					// Check if less file was written (outputs text if an error occured)
				if (strlen($result) != NULL) {
					$this->logMessage($result);
				} else {
						// set cache hash
					$cache->set($cacheKey,$contentHash,array());
				}
			} catch(Exception $e) {
				$errorMessage = $e->getMessage();
				$this->logMessage($errorMessage);
			}
		}

			// Add less as css
		$params['cssFiles'][$cssRelativeFilename] = array(
			'file' => $cssRelativeFilename,
			'rel' => 'stylesheet',
			'media' => 'all',
		);

		$memUsage = memory_get_usage() - $memUsage;
		$time_elapsed = microtime(true) - $start;

		if (file_exists($cssAbsoluteFilename)) {
			$outputCssModified = filemtime($cssAbsoluteFilename);
		}

			// Check output css file
		if ( $outputCssModified < $functionStarted && ($contentHashCache == '' || $contentHashCache != $contentHash)) {
			$errorMessage = 'WARNING, looks loke the css file wasn\'t saved properly (\'' . $cssAbsoluteFilename . '\' modified \'' . date ("F d Y H:i:s.", $outputCssModified) . '\')';
			$this->logMessage($errorMessage);
		}
		
			// Output debug information if debug is enabled
		if ((integer)$this->configuration['debug'] > 0 && TYPO3_MODE == 'FE' && $isDevelopIp) {

			$debugData = array();
			$debugData['CONFIGURATION'] = $this->configuration;
			$debugData['CSS'] = array(
				'cssOutputDir' => $cssOutputDir,
				'cssFilename' => $cssFilename,
				'cssRelativeFilename' => $cssRelativeFilename,
				'cssAbsoluteFilename' => $cssAbsoluteFilename,
			);
			$debugData['CACHE'] = array(
				'cacheKey' => $cacheKey,
				'contentHash' => $contentHash,
				'contentHashCache' => $contentHashCache,
				'TryToGenerateNewFile' => ($contentHashCache == '' || $contentHashCache != $contentHash),
			);
			$debugData['CSS FILES'] = $params['cssFiles'];
			$debugData['LESS FILES'] = $this->lessFiles;
			$debugData['MEMORY_USAGE'] = $this->convert($memUsage);
			$debugData['MEMORY_USAGE_TOTAL'] = $this->convert(memory_get_usage());
			$debugData['TIME_ELAPSED'] = $time_elapsed;
			$debugData['COMPILE STARTED'] = date ("F d Y H:i:s.", $functionStarted);
			$debugData['OUTPUT CSS LAST MODIFIED'] = date ("F d Y H:i:s.", $outputCssModified);
			$debugData['ERRORS'] = $this->errors;

			\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($debugData);

		}
	}

	/**
	 * Calculating relative path between the less file and compiled css file
	 *
	 * @param string $from Existing less file absolute path
	 * @param string $to To be written css file absolute path
	 * @return string Relative path
	 *
	 */
	private function getRelativePath($from, $to)
	{
			$from = explode('/', $from);
			$to = explode('/', $to);
			$relPath  = $to;

			foreach($from as $depth => $dir) {
					// find first non-matching dir
					if($dir === $to[$depth]) {
							// ignore this directory
							array_shift($relPath);
					} else {
							// get number of remaining dirs to $from
							$remaining = count($from) - $depth;
							if($remaining > 1) {
									// add traversals up to first matching dir
									$padLength = (count($relPath) + $remaining - 1) * -1;
									$relPath = array_pad($relPath, $padLength, '..');
									break;
							} else {
									$relPath[0] = './' . $relPath[0];
							}
					}
			}
			unset($relPath[count($relPath)-1]);
			return implode('/', $relPath);
	}

	/**
	 * Log messages
	 *
	 * @param string $errorMessage the string to log
	 * @return void
	 *
	 */
	protected function logMessage($errorMessage) {

			// Log to syslog (if set in install tool)
		TYPO3\CMS\Core\Utility\GeneralUtility::sysLog('Less_Parser : ' . $errorMessage, 'pxa_bootstrap', TYPO3\CMS\Core\Utility\GeneralUtility::SYSLOG_SEVERITY_ERROR);
		if ( (integer)$this->configuration['debug'] > 0 ) {
			$this->errors[] = $errorMessage;
		}

	}

	/**
	 * Go thru less files in configuration and store raw content in variable
	 *
	 * @param string $cssAbsoluteFilename the absolute filename where css will be written
	 * @return string
	 *
	 */
	protected function loadLessFilesContent($cssAbsoluteFilename) {

		$lessContent = '';
		$lessContentVariablesSection = '';

			// go thru all less files and add them to string that will be parsed
		foreach ($this->configuration['includes'] as $section => $item) {
			foreach ($item as $key => $file) {

					// get absoulute filename
				$absoluteFilename = TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($file);

					// verify that the file exists
				if (file_exists($absoluteFilename)) {

						// verify that the file is allowed
					if (TYPO3\CMS\Core\Utility\GeneralUtility::isAllowedAbsPath($absoluteFilename)) {

							// get file content and add to variable
						$fileContent = @file_get_contents($absoluteFilename);

							// fix the background images
						if ($this->configuration['cssUrlPrefix']['enabled'] == 1) {

								// First up two directories to root because generated css will be in typo3temp/pxa_bootstrap.
							$prefix = "../../" . $this->configuration['cssUrlPrefix']['prefix'];
								// Find and repclace all url:s when not include protocol.
							$search = '#url\((?!\s*[\'"]?(?:https?:)?//)\s*([\'"])?#';
							$replace = "url($1{$prefix}/";
							$fileContent = preg_replace($search, $replace, $fileContent);

						} else {
							$relativePath = $this->getRelativePath($cssAbsoluteFilename, $absoluteFilename);
							$fileContent = str_replace(array("url('..","url(..","url(\".."), array("url('" . $relativePath . '/..',"url(" . $relativePath . '/..',"url(\"" . $relativePath . '/..'), $fileContent);
						}

							// store debuginformation if debug is enabled
						if ( (integer)$this->configuration['debug'] > 1 ) {
								// Some parameters for measurment
							$memUsage = memory_get_usage();

								// try to parse content when debugging
							try {
									// Create Less_Parser Object
								$parser = new Less_Parser();

									// Try to parse lesscontent and the filecontent to see if something when wrong
								$stringToParse = $lessContentVariablesSection . $fileContent;

									// Add content to variable if it is in section variables
								$sectionsToKeep = array('variables', 'coreCss', 'mixins', 'utilityClasses');
								if ( in_array( $section, $sectionsToKeep) ) {
									$lessContentVariablesSection.=$fileContent;
								}

								$parser->parse($stringToParse, $GLOBALS['TSFE']->baseUrl);

									// get parsed css
								$this->lessFiles[$file]['content']['css'] = $parser->getCss();
								if ($section != 'variables' && strlen(trim($this->lessFiles[$file]['content']['css'])) == 0 ) {
									$this->logMessage("Might be a an error in file '" . $absoluteFilename . "', no css was outputted from parsed.");
								}

							} catch(Exception $e) {
								$errorMessage = $e->getMessage();
								$this->lessFiles[$file]['ERROR'] = $errorMessage;
								$this->logMessage("Might be a parser error '" . $errorMessage . "' in file '" . $absoluteFilename . "'");
							}
							$this->lessFiles[$file]['relativePath'] = $relativePath;
							$this->lessFiles[$file]['content']['less'] = $fileContent;

							$memUsage = memory_get_usage() - $memUsage;
							$this->lessFiles[$file]['memory_usage'] = $this->convert($memUsage);
						}

							// add to return variable
						$lessContent .= $fileContent;

					} else {
						$this->logMessage("Trying to include less file that isn't allowed (" . $absoluteFilename . ")");	
					}
				} else {
					$this->logMessage("Trying to include less file that doesn't exist (" . $absoluteFilename . ")");
				}
			}
		}
		return $lessContent;
	}

	function convert($size)	{

		$isNegative = false;

		if ( $size < 0 ) {
			$isNegative = true;
			$size = $size * -1;
		}

		$unit=array('b','kb','mb','gb','tb','pb');

		$retval = @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];

		if ( $isNegative ) {
			$retval = '-' . $retval;
		}

		return $retval;
	}

}
?>