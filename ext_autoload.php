<?php
$extensionClassesPath = t3lib_extMgm::extPath('pxa_bootstrap') . 'Classes/';
$extensionPath = t3lib_extMgm::extPath('pxa_bootstrap');

$default = array(
	'tx_pxabootstrap_mediarenderer_video_youtube' => $extensionClassesPath . 'MediaRenderer/Video/Youtube.php',
	'Less_Parser' => $extensionPath . 'Resources/Public/Contrib/less.php/lib/Less/Parser.php',
	'Less_Autoloader' => $extensionPath . 'Resources/Public/Contrib/less.php/lib/Less/Autoloader.php',
);

return $default;
?>