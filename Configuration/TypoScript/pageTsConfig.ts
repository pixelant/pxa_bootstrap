#### TCEFORM ####
TCEFORM {
    pages {
        layout.disabled = 1
    }
    tt_content {
		image_frames.disabled = 0
		
		section_frame {
			removeItems = 10,11,12,20,21
			addItems {
				67 = LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.section_frame.I.67
				111 = LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.section_frame.I.111
				113 = LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:tt_content.section_frame.I.113
			}
		}

        header_layout {
            altLabels {
                1 = H1
                2 = H2
                3 = H3
                4 = H4
                5 = H5
            }
        }
        layout {
            disableNoMatchingValueElement = 1
            types {
                bullets {
                    removeItems = 0,1,2,3
                    addItems {
                        100 = unordered
                        110 = ordered
                        120 = unstyled
                        130 = inline
                    }
                }
                table {
                    removeItems = 0,1,2,3
                }
            }
        }
        imagecols {
            removeItems = 5,7,8
        }
        table_bgColor.disabled = 1
        table_border.disabled = 1
        table_cellspacing.disabled = 1
        table_cellpadding.disabled = 1
        pi_flexform {
            table {
                sDEF {
                    acctables_nostyles.disabled = 1
                    acctables_tableclass.disabled = 0
                }
            }
        }
    }
}
