# *****************
# Add constatns for logo and layout
# *****************
tx_pxabootstrap.config {
	logo {
		maxW = 200
		maxH = 30
		file = EXT:pxa_bootstrap/Resources/Public/Images/logo.png
	}
	siteName = The Site Name
	layout {
		useLayout = Default
		useHeader = Default
		useFooter = Default
	}
}