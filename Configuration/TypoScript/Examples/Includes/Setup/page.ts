##### Make the PAGE object #####
page = PAGE
page {
	# Regular pages always have typeNum = 0
	typeNum = 0
}

# Create a Fluid Template
page.10 = FLUIDTEMPLATE
page.10 {
    # Set the Template Pathes
    partialRootPath = EXT:pxa_bootstrap/Resources/Private/Partials/
    layoutRootPath = EXT:pxa_bootstrap/Resources/Private/Layouts/
  
    variables {
    
        content_col_0 < styles.content.get
        
        content_col_1 < styles.content.get
        content_col_1.select.where = colPos = 1

        content_col_2 < styles.content.get
        content_col_2.select.where = colPos = 2

        logo < templates.navbar.brand

        #main_navigation < templates.main-navigation-collapsible
        main_navigation < templates.main-navigation-collapsible-fixed-top
        #main_navigation < templates.main-navigation-collapsible-static-top

        submenu <  templates.submenu
        
        breadcrumbs < templates.menu.breadcrumbs


        
    }
      # Add Settings to fluidtemplate from ts settings
    settings {
      layout < plugin.tx_pxabootstrap.settings.layout
    }
}

  # Assign the Template files with the Fluid Backend-Template
page.10.file.stdWrap.cObject = CASE
page.10.file.stdWrap.cObject {
  
  key.data = levelfield:-1, backend_layout_next_level, slide
  key.override.field = backend_layout
    # Set the default Template (not using be layout here)
  default = TEXT
  default.value = EXT:pxa_bootstrap/Resources/Private/Templates/Subpage.html

}
  # As an example override template on root-page to Default
[treeLevel = TSFE:id = leveluid:0]
page.10.file.stdWrap.cObject {
  default.value = EXT:pxa_bootstrap/Resources/Private/Templates/Default.html
}  
[global]
