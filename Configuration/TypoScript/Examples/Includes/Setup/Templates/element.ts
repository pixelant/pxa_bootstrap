templates.element {
	navbar-form-search = COA
	navbar-form-search.wrap = <form class="navbar-form navbar-right" role="search">|</form>
	navbar-form-search {
		
		10 = TEXT
		10.wrap = <div class="form-group">|</div>
		10.value = <input type="text" class="form-control" placeholder="Search">

		20 = TEXT
		20.value = <button type="submit" class="btn btn-default">Submit</button>
	}
}

templates {
	
	main-navigation-collapsible = COA
	main-navigation-collapsible.10 < templates.navbar.navbar-default
	main-navigation-collapsible.10 {
		10 < templates.navbar.header
		10 {
			10 < templates.navbar.button
			20 < templates.navbar.brand
		}
		20 < templates.menu.nav-navbar-nav-collapsible
		20.entryLevel = 0
	}

	main-navigation-collapsible-fixed-top = COA
	main-navigation-collapsible-fixed-top.10 < templates.navbar.navbar-default-fixed-top
	main-navigation-collapsible-fixed-top.10 {
		10 < templates.navbar.header
		10 {
			10 < templates.navbar.button
			20 < templates.navbar.brand
		}
		20 < templates.menu.nav-navbar-nav-collapsible
		20.entryLevel = 0
	}

	main-navigation-collapsible-static-top = COA
	main-navigation-collapsible-static-top.10 < templates.navbar.navbar-default-static-top
	main-navigation-collapsible-static-top.10 {
		10 < templates.navbar.header
		10 {
			10 < templates.navbar.button
			20 < templates.navbar.brand
		}
		20 < templates.menu.nav-navbar-nav-collapsible
		20.entryLevel = 0
	}

	submenu <  templates.menu.nav-nav-pills-nav-stacked
	submenu {
		wrap = <ul class="nav submenu">|</ul>
		entryLevel = 1
		1  {
			IFSUB {
				wrapItemAndSub = <li>|</li> |*| <li>|</li> |*| <li class="last">|</li>
				ATagParams >
				stdWrap.wrap >
			}
			ACTIFSUB < .IFSUB
			ACTIFSUB {
				wrapItemAndSub = <li class="active">|</li> |*| <li class="active">|</li> |*| <li class="active last">|</li>
			}
			CURIFSUB < .ACTIFSUB
		}
		2 {
			wrap = <ul class="nav">|</ul>
		}
		3 < .2
		4 < .3
		5 < .4
	}

}