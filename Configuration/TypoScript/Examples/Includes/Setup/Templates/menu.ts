# *****************
# # A MENU template.
# *****************
templates.menu {
	
	nav-navbar-nav = HMENU
	nav-navbar-nav {
		wrap = <ul class="nav navbar-nav">|</ul>
		1 = TMENU
		1 {
			expAll = 1
			NO = 1
			NO {
				ATagTitle {
					field = title
					fieldRequired = nav_title
				}
				wrapItemAndSub = <li>|</li> |*| <li>|</li> |*| <li class="last">|</li>
				stdWrap.htmlSpecialChars = 1
			}
			IFSUB < .NO
			IFSUB {
				wrapItemAndSub = <li class="dropdown">|</li> |*| <li class="dropdown">|</li> |*| <li class="dropdown last">|</li>
				ATagParams = class="dropdown-toggle" data-toggle="dropdown"
				stdWrap.wrap = |<span class="caret"></span>
				#doNotLinkIt = 1
			}
			ACTIFSUB < .IFSUB
			ACTIFSUB {
				wrapItemAndSub = <li class="dropdown active">|</li> |*| <li class="dropdown active">|</li> |*| <li class="dropdown active last">|</li>
				ATagParams = class="dropdown-toggle" data-toggle="dropdown"
				stdWrap.wrap = |<span class="caret"></span>
				#doNotLinkIt = 1
			}
			CURIFSUB < .IFSUB
			ACT < .NO
			ACT {
				wrapItemAndSub = <li class="active">|</li> |*| <li class="active">|</li> |*| <li class="active last">|</li>
				ATagParams = class="active"
			}
			CUR < .NO
			CUR {
				wrapItemAndSub = <li class="active">|</li> |*| <li class="active">|</li> |*| <li class="active last">|</li>
				ATagParams = class="selected"
			}
		}
		2 = TMENU
		2 {
			wrap = <ul class="dropdown-menu">|</ul>
			#expAll = 1
			NO = 1
			NO {
				ATagTitle {
					field = title
					fieldRequired = nav_title
				}
				wrapItemAndSub = <li>|</li> |*| <li>|</li> |*| <li class="last">|</li>
				stdWrap.htmlSpecialChars = 1
			}
			ACT < .NO
			ACT {
				wrapItemAndSub = <li class="active">|</li> |*| <li class="active">|</li> |*| <li class="active last">|</li>
				ATagParams = class="active"
			}
			CUR < .NO
			CUR {
				wrapItemAndSub = <li class="active">|</li> |*| <li class="active">|</li> |*| <li class="active last">|</li>
				ATagParams = class="selected"
			}
			SPC < .NO
			SPC {
				wrapItemAndSub = <li class="divider active">|</li> |*| <li class="divider active">|</li> |*| <li class="divider active last">|</li>
				doNotLinkIt = 1
			}
		}
	}

	nav-nav-tabs < .nav-navbar-nav
	nav-nav-tabs.wrap = <ul class="nav nav-tabs">|</ul>

	nav-nav-tabs-nav-justified < .nav-navbar-nav
	nav-nav-tabs-nav-justified.wrap = <ul class="nav nav-tabs nav-justified">|</ul>

	nav-nav-pills < .nav-navbar-nav
	nav-nav-pills.wrap = <ul class="nav nav-pills">|</ul>

	nav-nav-pills-nav-stacked < .nav-navbar-nav
	nav-nav-pills-nav-stacked.wrap = <ul class="nav nav-pills nav-stacked">|</ul>

	nav-navbar-nav-collapsible < .nav-navbar-nav
	nav-navbar-nav-collapsible.wrap = <div class="collapse navbar-collapse" id="navbar-collapse-1"><ul class="nav navbar-nav">|</ul></div>
}

# *****************
# For the breadcrumb cObject we use a HMENU of the type 'rootline'
# *****************
templates.menu.breadcrumbs = COA
templates.menu.breadcrumbs {
	10 = HMENU
	10 {
		wrap = <ol class="breadcrumb">|</ol>
  		special = rootline
		special.range = 0|-1
		includeNotInMenu = 1
		1 = TMENU
		1 {
			NO = 1
			NO.allWrap = <li>|</li>
			NO.stdWrap.htmlSpecialChars = 1
			ACT < .NO
			ACT {
				NO.allWrap = <li class="active">|</li>
			}
		}
	}
}
	# if it is a single news
[globalVar = GP:tx_news_pi1|news > 0]
templates.menu.breadcrumbs {
	10.1 {
		CUR = 1
		CUR < .NO
	}
	20 = RECORDS
	20 {
		if.isTrue.data = GP:tx_news_pi1|news
		dontCheckPid = 1
		tables = tx_news_domain_model_news
		source.data = GP:tx_news_pi1|news
		source.intval = 1
		conf.tx_news_domain_model_news = TEXT
		conf.tx_news_domain_model_news {
			field = title
			htmlSpecialChars = 1
			typolink {
				parameter.data = page:uid
				addQueryString = 1
			}
		}
		wrap =  <li>|</li>
	}
}
[else]
templates.menu.breadcrumbs {
	10.1 {
		CUR = 1
		CUR.stdWrap.htmlSpecialChars = 1
		CUR.allWrap = <li>|</li>
		CUR.doNotLinkIt = 1
	}
}
[global]