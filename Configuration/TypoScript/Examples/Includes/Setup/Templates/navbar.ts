# *****************
# navbar template
# *****************
templates {
	navbar {

		navbar-default = COA
		navbar-default {
			wrap = <nav class="navbar navbar-default" role="navigation">|</nav>
		}

		navbar-default-fixed-top < .navbar-default
		navbar-default-fixed-top {
			wrap = <nav class="navbar navbar-default navbar-fixed-top" role="navigation"><div class="container">|</div></nav>
		}

		navbar-default-static-top < .navbar-default
		navbar-default-static-top {
			wrap = <nav class="navbar navbar-default navbar-static-top" role="navigation">|</nav>
		}
		
		button = COA
		button.wrap = <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">|</button>
		button {

			10 = TEXT 
			10.value = <span class="sr-only">Toggle navigation</span>

        	20 = TEXT 
        	20.value = <span class="icon-bar"></span>
        
			30 < .20

			40 < .20

		}		  
            
      	header = COA
      	header.wrap = <div class="navbar-header">|</div>

      		# Fetches logo.png from filepath image
		brand = IMAGE
		brand {
			file = {$tx_pxabootstrap.config.logo.file}
			file.maxH = {$tx_pxabootstrap.config.logo.maxH}
			file.maxW = {$tx_pxabootstrap.config.logo.maxW}
			altText = {$tx_pxabootstrap.config.siteName}
			altText.insertData = 1
			imageLinkWrap = 1
			imageLinkWrap {
				enable = 1
				typolink.parameter.data = leveluid:0
				typolink.ATagParams = class="navbar-brand"
			}
		}
		
			# Fetches image from page media
		brand-media-from-page = FILES
		brand-media-from-page {
			references.data = levelmedia:-1,slide
			references.listNum = 0
			renderObj = IMAGE
			renderObj {
				file.import.data = file:current:publicUrl
				file.maxH = {$tx_pxabootstrap.config.logo.maxH}
				file.maxW = {$tx_pxabootstrap.config.logo.maxW}			
				altText.data = file:current:title
				altText.insertData = 1
				imageLinkWrap = 1
				imageLinkWrap {
					enable = 1
					typolink.parameter.data = leveluid:0
					typolink.ATagParams = class="navbar-brand"
				}
			}
		}
	}
}

