# customsubcategory=aa_less=LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:constants.label.category.less
# customsubcategory=bb_libs=LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:constants.label.category.libs
# customsubcategory=hh_debug=LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:constants.label.category.debug


tx_pxabootstrap.config {
	# cat=plugin.tx_pxabootstrap/aa_less/aa; type=boolean; label=LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:constants.label.disableLessCompilation
	disableLessCompilation = 0
	
	# cat=plugin.tx_pxabootstrap/aa_less/ab; type=boolean; label=LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:constants.label.compress
	compress = 1

	# cat=plugin.tx_pxabootstrap/aa_less/ac; type=string; label=LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:constants.label.cssUrlPrefixEnable
	cssUrlPrefixEnable =

	# cat=plugin.tx_pxabootstrap/aa_less/ad; type=string; label=LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:constants.label.cssUrlPrefix
	cssUrlPrefix =

	# cat=plugin.tx_pxabootstrap/aa_less/ae; type=string; label=LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:constants.label.outputCssFilename
	outputCssFilename =

	# cat=plugin.tx_pxabootstrap/bb_libs/ba; type=boolean; label=LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:constants.label.includeTYPO3ContribJquery
	includeTYPO3ContribJquery = 1

	# cat=plugin.tx_pxabootstrap/bb_libs/bb; type=boolean; label=LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:constants.label.includeBootstrapJsInFooterlibs
	includeBootstrapJsInFooterlibs = 1
		
	# cat=plugin.tx_pxabootstrap/hh_debug/hh; type=options[None=0,Simple=1,Extensive=2]; label=LLL:EXT:pxa_bootstrap/Resources/Private/Language/locallang_db.xlf:constants.label.displayDebugData
	displayDebugData = 0

}
# *****************
# change constant to set a login template modified with bootstrap classes
# *****************
styles.content.loginform.templateFile = EXT:pxa_bootstrap/Resources/Private/Extensions/felogin/template.html
