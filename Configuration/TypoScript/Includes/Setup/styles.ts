######################################
#### UNSET CSS STYLED CONTENT CSS ####
######################################
plugin.tx_cssstyledcontent >


##############
#### MENU ####
##############
tt_content.menu.20.default.stdWrap.outerWrap = <ul class="nav">|</ul>
tt_content.menu.20.1.stdWrap.outerWrap = <ul class="nav">|</ul>

###########################
#### LIB PARSEFUNC RTE ####
###########################
lib.parseFunc_RTE {

	externalBlocks := addToList(hgroup)
    
    externalBlocks {
        blockquote.callRecursive.tagStdWrap.HTMLparser.tags.blockquote.overrideAttribs >

        	# Set default class of tables, and restrict allowed classes
		table.stdWrap.HTMLparser.tags.table.fixAttrib.class {
			default = table
			always = 1
			list = table,table-striped,table-bordered,table-hover,table-condensed
		}
    }
	nonTypoTagStdWrap.encapsLines.addAttributes.P.class =
}

#######################
#### LIB STDHEADER ####
#######################
lib.stdheader {
    3 {
        headerClass {
            cObject {
                10 {
                    field = header_position
                    required = 1
                    noTrimWrap = |text-| |
                }
                20 {
                    value =
                }
            }
        }
    }
    5 >
    #10 >
    #10 = CASE
    10 {
        setCurrent {
            field = header
            htmlSpecialChars = 1
            typolink.parameter.field = header_link
        }
        key {
            field = header_layout
            ifEmpty = {$content.defaultHeaderType}
            ifEmpty.override.data = register: defaultHeaderType
        }
        1 = COA
        1 {
            10 = TEXT
            10.current = 1
            20 = TEXT
            20 {
                field = subheader
                stdWrap.noTrimWrap = | <small>|</small>|
                stdWrap.required = 1
            }
            stdWrap.dataWrap = <h1{register:headerClass}>|</h1>
            stdWrap.required = 1
            dataWrap >
        }
    2 < .1
    2.stdWrap.dataWrap = <h2{register:headerClass}>|</h2>
    3 < .1
    3.stdWrap.dataWrap = <h3{register:headerClass}>|</h3>
    4 < .1
    4.stdWrap.dataWrap = <h4{register:headerClass}>|</h4>
    5 < .1
    5.stdWrap.dataWrap = <h5{register:headerClass}>|</h5>
    }
    20 >
    40 >
    40 = COA
    40 {
        stdWrap {
            wrap = <p>|</p>
            innerWrap {
                cObject = COA
                cObject {
                    10 = TEXT
                    10 {
                        value = <time
                    }
                    20 = TEXT
                    20 {
                        noTrimWrap = | datetime="|"|
                        field = date
                        strftime = %Y-%m-%d
                    }
                    30 = TEXT
                    30 {
                        value = >|</time>
                    }
                }
            }
            required = 1
        }
        10 = TEXT
        10 {
            field = date
            strftime = %Y-%m-%d

        }
        if {
            isTrue {
                field = date
            }
    }
    }
    stdWrap {
        dataWrap >
        dataWrap = |
    }
    stdWrap.prefixComment >
}

################################################
#### CTYPE: HEADER                          ####
################################################
tt_content.header.20 >

#######################
#### CTYPE: BULLET ####
#######################
tt_content.bullets.20.dataWrap >
tt_content.bullets.20.dataWrap.cObject = CASE
tt_content.bullets.20.dataWrap.cObject {
    key.field = layout
    default = COA
    default {
        10 = TEXT
        10 {
            value = <ul
        }
        20 = COA
        20 {
            10 = TEXT
            10 {
                value = list
                required = 1
                noTrimWrap = || |
            }
            stdWrap {
                trim = 1
                noTrimWrap = | class="|"|
                required = 1
            }
        }
        30 = TEXT
        30 {
            value = >|</ul>
        }
    }
    110 =< tt_content.bullets.20.dataWrap.cObject.default
    110.10.value = <ol
    110.30.value =  >|</ol>
    120 =< tt_content.bullets.20.dataWrap.cObject.default
    120.20.10.value = list list-unstyled
    130 =< tt_content.bullets.20.dataWrap.cObject.default
    130.20.10.value = list list-inline

    	# Include styles for "original" styles
	1 =< tt_content.bullets.20.dataWrap.cObject.110
	2 =< tt_content.bullets.20.dataWrap.cObject.120
	3 =< tt_content.bullets.20.dataWrap.cObject.130

}

#########################
#### LIB STDHEADER 8 ####
#########################
lib.stdheader.10.8 < lib.stdheader.10.1
lib.stdheader.10.8.dataWrap = <div class="page-header"><h2{register:headerClass}>|</h2></div>
########################
#### CONTENT FRAMES ####
########################

// Responsive Utility Class
tt_content.stdWrap.innerWrap.cObject.default.20.15 = TEXT
tt_content.stdWrap.innerWrap.cObject.default.20.15 {
	space = before
	field = tx_pxabootstrap_responsive_utility_class
	required = 1
	noTrimWrap = || |
}

// Fix, when "No Frame" is selected and no space before or space after, dont render "space" div.
tt_content.stdWrap.innerWrap.cObject.66 {
	20.10.stdWrap.required = 1
	40.10.stdWrap.required = 1
	// Add a wrapper div around content if a "Responsive Utility Class" is selected
	wrap = <div class="{field:tx_pxabootstrap_responsive_utility_class}">|</div>
	wrap.insertData = 1
	wrap.if {
		isTrue.field = tx_pxabootstrap_responsive_utility_class
	}
}


	# Add frame No Frame With ruler after
tt_content.stdWrap.innerWrap.cObject.67 =< tt_content.stdWrap.innerWrap.cObject.66
tt_content.stdWrap.innerWrap.cObject.67 {
	50 = TEXT
	50.value = <hr />
}


	# Add frame Article
tt_content.stdWrap.innerWrap.cObject.111 =< tt_content.stdWrap.innerWrap.cObject.default
tt_content.stdWrap.innerWrap.cObject.111 {
	20.10.value = article
	10.cObject.default.value = <article id="c{field:uid}"
	30.cObject.default.value = >|</article>
}

	# Add frame Jumbotron
tt_content.stdWrap.innerWrap.cObject.113 =< tt_content.stdWrap.innerWrap.cObject.default
tt_content.stdWrap.innerWrap.cObject.113 {
	20.10.value = jumbotron
}

	# Add real hr after frame "Ruler After"
tt_content.stdWrap.innerWrap.cObject.6 {
	30.cObject.default.value = >|</div><hr />
	20.10.value = 
}

	# Add real hr before frame "Ruler Before"
tt_content.stdWrap.innerWrap.cObject.5 {
	10.cObject.default.value = <hr /><div id="c{field:uid}" 
	20.10.value >
}

# Frame invisible
tt_content.stdWrap.innerWrap.cObject.1.20.10.value = hidden


########################
#### CTYPE: UPLOADS ####
########################
# Default   : Just filename
# Layout 1  : Icon + filename
# Layout 2  : Icon + filename + description
# Layout 3  : Thumbnail + filename + description
# Optional  : Display filesize
tt_content.uploads.20 {

        # Build a link to be used as a wrap for the row instead of in every part
    tempLink = TEXT
    tempLink {          
        typolink {
            parameter.data = file:current:originalUid // file:current:uid
            parameter.wrap = file:|
            fileTarget < lib.parseTarget
            fileTarget =
            fileTarget.override = {$styles.content.uploads.target}
            fileTarget.override.override.field = target
            removePrependedNumbers = 1
        }
    }
    
        # Modify renderObj
    renderObj {
        
            # Modify thumbnail
        10 {
            stdWrap {
                    # Remove link
                typolink >
                    # Only on layout 3
                if.value = 2
            }
                # change the wrap
            wrap = <div class="pull-left uploads-image">|</div>
        }
        
            # Modify icon
        15 {
            stdWrap {
                    # Remove link
                typolink > 
                if {
                        # only show the icon if we don't have a thumbnail
                    isFalse.cObject = IMG_RESOURCE
                    isFalse.cObject.file < tt_content.uploads.20.renderObj.10.file
                    isFalse.cObject.stdWrap.if < tt_content.uploads.20.renderObj.10.stdWrap.if
                }       
            }
                # Change the wrap
            wrap = <div class="pull-left uploads-icon">|</div>
        }

            # Modify filename
        20 {
                # Remove link
            typolink >
                # Change so we use title in file if it exists or else use the filename
            data = file:current:title // file:current:name
                # Change wrap
            wrap = <p class="list-group-item-heading uploads-filename">|</p>
                # Remove file-extension
            replacement.20 {
                        search = /(.*)(\..*)/
                        replace = \1
                        useRegExp = 1
            }
        }

            # Modify description
        30 {
                # Change wrap
            wrap = <p class="list-group-item-text uploads-description">|</p>
            stdWrap {
                    # replace newline with br
                br = 1
                    # only display on layouts 
                if.value = 1
                if.isGreaterThan.field = layout
            }
                # allow html so we can display <br /> tags
            htmlSpecialChars = 0    
        }
        
            # Modify filesize
        40 {
                # Change the wrap
            wrap = <p class="list-group-item-text uploads-filesize">|</p>
        }
        
            # wrap row with link instead
        wrap.cObject {
            20 {
                value = |
                typolink < tt_content.uploads.20.tempLink.typolink
                typolink.ATagParams = class="list-group-item"
                typolink.ATagParams.insertData = 1
            }
        }   
    }
    
        # Wrap output of file uploads list with twitter bootstrap class
    stdWrap {
            # Change the wrap
        dataWrap = <div class="list-group uploads uploads-layout{field:layout}">|</div>
    }
}


########################
#### CTYPE: IMAGE ######
########################
# Change params for image by image_frames selection
# Remove classes and wraps around images
tt_content.image.20.rendering {
    
    singleNoCaption.allStdWrap.wrap = | 
    singleNoCaption.allStdWrap.innerWrap.cObject.0.value = |
    singleNoCaption.allStdWrap.innerWrap.cObject.8.value = |
    singleNoCaption.singleStdWrap.wrap = <figure class="figure">|</figure>

    noCaption.allStdWrap.wrap = |
    noCaption.singleStdWrap.wrap = |
    noCaption.rowStdWrap.wrap.override = <div class="row image-group"> | </div>
    noCaption.noRowsStdWrap.wrap.override = <div class="row image-group"> | </div>
    noCaption.lastRowStdWrap.wrap.override = <div class="row image-group"> | </div>
    noCaption.columnStdWrap.wrap.override = <div>|</div>
    noCaption.columnStdWrap.wrap.override.cObject = CASE
    noCaption.columnStdWrap.wrap.override.cObject {
        key.field = imagecols
        # above-center
        default = TEXT
        default.value = <div>|</div>
        
        1 = TEXT
        1.value = <div class="col-sm-12">|</div>

        2 = TEXT
        2.value = <div class="col-sm-6">|</div>

        3 = TEXT
        3.value = <div class="col-sm-4">|</div>

        4 = TEXT
        4.value = <div class="col-sm-3">|</div>

        6 = TEXT
        6.value = <div class="col-sm-2">|</div>
    }
    
    singleCaption.singleStdWrap.wrap.override = <figure class="figure">|###CAPTION###</figure>
    singleCaption.caption.wrap.override = <figcaption class="caption###CLASSES###">|</figcaption> 
    
    splitCaption.singleStdWrap.wrap.override = <figure class="figure">|###CAPTION###</figure>
    splitCaption.rowStdWrap.wrap.override = <div class="row image-group"> | </div>
    splitCaption.noRowsStdWrap.wrap.override = <div class="row image-group"> | </div>
    splitCaption.lastRowStdWrap.wrap.override = <div class="row image-group"> | </div>
    splitCaption.columnStdWrap.wrap.override = <div>|</div>
    splitCaption.columnStdWrap.wrap.override.cObject = CASE
    splitCaption.columnStdWrap.wrap.override.cObject {
        key.field = imagecols
        # above-center
        default = TEXT
        default.value = <div>|</div>
        
        1 = TEXT
        1.value = <div class="col-sm-12">|</div>

        2 = TEXT
        2.value = <div class="col-sm-6">|</div>

        3 = TEXT
        3.value = <div class="col-sm-4">|</div>

        4 = TEXT
        4.value = <div class="col-sm-3">|</div>

        6 = TEXT
        6.value = <div class="col-sm-2">|</div>
    }
    splitCaption.caption.wrap.override = <figcaption class="caption###CLASSES###">|</figcaption> 

    globalCaption.allStdWrap.wrap.override = <figure class="figure">|###CAPTION###</figure>
    globalCaption.singleStdWrap.wrap = |
    globalCaption.rowStdWrap.wrap.override = <ul class="gallery"> | </ul>
    globalCaption.noRowsStdWrap.wrap.override = <ul class="gallery"> | </ul>
    globalCaption.lastRowStdWrap.wrap.override = <ul class="gallery"> | </ul>
    globalCaption.columnStdWrap.wrap.override = <li>globalCaption | </li>
    globalCaption.caption.wrap.override = <figcaption class="caption###CLASSES###"> | </figcaption>

}

# Default possible values in image position selection:
# <option value="1">Ovanför, till höger</option>
# <option value="2">Ovanför, till vänster</option>
# <option value="8">Under, i mitten</option>
# <option value="9">Under, till höger</option>
# <option value="10">Under, till vänster</option>
# <option value="17">Till höger i texten</option>
# <option value="18">Till vänster i texten</option>
# <option value="25">Till höger om texten</option>
# <option value="26">Till vänster om texten</option>

# Single Caption - Change class on the figure tag aroun images, so we can handle alignment from figure tag 
tt_content.image.20.rendering.singleCaption.singleStdWrap.wrap.override.cObject = COA 
tt_content.image.20.rendering.singleCaption.singleStdWrap.wrap.override.cObject {
    10 = CASE
    10 {
        key.field = imageorient
        
        default = TEXT
        default.value = <figure class="figure figure-center">|###CAPTION###</figure>

        1 = TEXT
        1.value = <figure class="figure-right figure-above">|###CAPTION###</figure>

        2 = TEXT
        2.value = <figure class="figure-left figure-above">|###CAPTION###</figure>

        8 = TEXT
        8.value = <figure class="figure figure-center figure-below">|###CAPTION###</figure>

        9 = TEXT
        9.value = <figure class="figure-right figure-below">|###CAPTION###</figure>

        10 = TEXT
        10.value = <figure class="figure-left figure-below">|###CAPTION###</figure>

        17 = TEXT
        17.value = <figure class="figure-right">|###CAPTION###</figure>

        18 = TEXT
        18.value = <figure class="figure-left">|###CAPTION###</figure>

        25 = TEXT
        25.value = <figure class="figure-right figure-beside">|###CAPTION###</figure>

        26 = TEXT
        26.value = <figure class="figure-left figure-beside">|###CAPTION###</figure>
    }
}

# Single No Caption - Change class on the figure tag aroun images, so we can handle alignment from figure tag 
tt_content.image.20.rendering.singleNoCaption.singleStdWrap.wrap.override.cObject = COA 
tt_content.image.20.rendering.singleNoCaption.singleStdWrap.wrap.override.cObject {
    10 = CASE
    10 {
        key.field = imageorient
        
        default = TEXT
        default.value = <figure class="figure figure-center">|</figure>

        1 = TEXT
        1.value = <figure class="figure-right figure-above">|</figure>

        2 = TEXT
        2.value = <figure class="figure-left figure-above">|</figure>

        8 = TEXT
        8.value = <figure class="figure figure-center figure-below">|</figure>

        9 = TEXT
        9.value = <figure class="figure-right figure-below">|</figure>

        10 = TEXT
        10.value = <figure class="figure-left figure-below">|</figure>

        17 = TEXT
        17.value = <figure class="figure-right">|</figure>

        18 = TEXT
        18.value = <figure class="figure-left">|</figure>

        25 = TEXT
        25.value = <figure class="figure-right figure-beside">|</figure>

        26 = TEXT
        26.value = <figure class="figure-left figure-beside">|</figure>
    }
}

# Change class on images width bootstrap classes
tt_content.image.20.1.params {
    cObject = COA
    cObject.wrap = class="|"
    cObject.10 = CASE 
    cObject.10 {
        key.field = image_frames
        
        default = TEXT
        default.value = 

        1 = TEXT
        1.value = img-rounded
        #1.noTrimWrap = | ||

        2 = TEXT
        2.value = img-circle
        #2.noTrimWrap = | ||

        3 = TEXT
        3.value = img-thumbnail
        #3.noTrimWrap = | ||        
    }
}

# Alter layout, no div wrap
tt_content.image.20 {
    layout {
        # above-center
        default.value = ###IMAGES######TEXT###
        # above-right
        1.value = ###IMAGES###<div class="clearfix"></div>###TEXT###
        # above-left
        2.value = ###IMAGES###<div class="clearfix"></div>###TEXT###
        # below-center
        8.value = ###TEXT######IMAGES###
        # below-right
        9.value = ###TEXT######IMAGES###<div class="clearfix"></div>
        # below-left
        10.value = ###TEXT######IMAGES###<div class="clearfix"></div>

        # intext-right
        17.value = ###IMAGES######TEXT###<div class="clearfix"></div>
        # intext-left
        18.value = ###IMAGES######TEXT###<div class="clearfix"></div>
        # beside-text-right
        25.value = ###IMAGES###<div class="beside-text beside-text-right"><div class="beside-text-inner">###TEXT###</div></div><div class="clearfix"></div>
        # beside-text-left
        26.value = ###IMAGES###<div class="beside-text beside-text-left"><div class="beside-text-inner">###TEXT###</div></div><div class="clearfix"></div>
    }
} 

# Remove wrap around image-text
tt_content.textpic.20.text.wrap = |

# Change the rel attribute to data-rel for lightbox js
tt_content.image.20.1.imageLinkWrap.linkParams.ATagParams.dataWrap =  class="{$styles.content.imgtext.linkWrap.lightboxCssClass}" data-rel="{$styles.content.imgtext.linkWrap.lightboxRelAttribute}"

#########################
#### CTYPE: MAILFORM ####
#########################
tt_content.mailform.20.stdWrap.wrap > 

# Add "dummy" ts layout for postProcessor mail so the "default" layout isn't used when included in mail because layout breaks html content.
tt_content.mailform.20.postProcessor.mail.layout.dummyToClearStyles = jajamensan

tt_content.mailform.20 {
    layout {
        form (
            <form role="form">
                <containerWrap />
            </form>
        )
        elementWrap (
            <div>
                <element />
            </div>
        )
        containerWrap (
            <div>
                <elements />
            </div>
        )
        button (
            <label />
            <input />
        )
        reset (
            <label />
            <input class="btn btn-default" />
        )
        textline (
            <label />
            <input class="form-control" />
        )
        textline (
            <label />
            <input class="form-control" />
        )
        password (
            <label />
            <input class="form-control" />
        )
        select (
            <label />
            <select class="form-control">
                <elements />
            </select>
        )
        textarea (
            <label />
            <textarea class="form-control" rows="3" />
        )
        fileupload (
            <label />
            <input />
        )
        checkbox (
            <div class="checkbox">
                <input />
                <label />
            </div>
        )
        radio (
            <div class="radio">
                <input />
                <label />
            </div>
        )
        submit (
            <div class="form-actions">
                <label />
                <input class="btn btn-primary" />
            </div>
            
        )
        mandatory (
            <span class="glyphicon glyphicon-asterisk"></span>
        )
        error (
            <div>
                <errorvalue />
            </div>
        )
    }
}
