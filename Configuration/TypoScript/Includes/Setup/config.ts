config {
	doctype = html5
	htmlTag_setParams = none

		// Character sets
	renderCharset = utf-8
	metaCharset = utf-8

		// Cache settings
	cache_period = 43200
	sendCacheHeaders = 1

		// Link settings
	prefixLocalAnchors = all

		// Remove targets from links
	intTarget =
	extTarget =

		// Code cleaning
	disablePrefixComment = 1

		// Move default CSS and JS to external file
	removeDefaultJS = external
	inlineStyle2TempFile = 1

		// Protect mail addresses from spamming
	spamProtectEmailAddresses = -3
	spamProtectEmailAddresses_atSubst = @<span style="display:none;">remove-this.</span>

	concatenateCss = 1
	concatenateJs = 1

	index_enable = 0
	linkVars = 	
}