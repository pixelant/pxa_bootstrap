# Style the paginator
plugin.tx_news {
	view {
		widget.Tx_News_ViewHelpers_Widget_PaginateViewHelper.templateRootPath = EXT:pxa_bootstrap/Resources/Private/Extensions/news/Templates/
	}
}