plugin.tx_felogin_pi1 {
	_CSS_DEFAULT_STYLE >

	// Add ATagParams to all links generated in FE-login
	linkConfig.ATagParams = class="pull-right btn btn-default"

	errorMessage_stdWrap.wrap = <div class="alert alert-danger">|</div>
	welcomeMessage_stdWrap.wrap = <div class="alert alert-info">|</div>
	successMessage_stdWrap.wrap = <div class="alert alert-success">|</div>
	forgotResetMessageEmailSentMessage_stdWrap.wrap = <div class="alert alert-info">|</div>
	logoutMessage_stdWrap.wrap = <div class="alert alert-info">|</div>
	forgotMessage_stdWrap.wrap = <div class="alert alert-info">|</div>
	changePasswordNotValidMessage_stdWrap.wrap = <div class="alert alert-danger">|</div>
	changePasswordTooShortMessage_stdWrap.wrap = <div class="alert alert-danger">|</div>
	changePasswordNotEqualMessage_stdWrap.wrap = <div class="alert alert-danger">|</div>
	changePasswordMessage_stdWrap.wrap = <div class="alert alert-info">|</div>
	changePasswordDoneMessage_stdWrap.wrap = <div class="alert alert-success">|</div>

}
