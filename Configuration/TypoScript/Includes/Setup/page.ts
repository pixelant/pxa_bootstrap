# *********************
# Include jQuery from TYPO3 Contrib
# *********************
[globalString = LIT:1 = {$tx_pxabootstrap.config.includeTYPO3ContribJquery}]
	page.javascriptLibs.jQuery = 1
	page.javascriptLibs.jQuery.noConflict = 0
[global]

# *********************
# include all bootstrap less files manually instead of using the bootstrap.less so we can add f.ex. out custom.less right after variables.less
# *********************
# Include in fluidcontent
plugin.tx_pxabootstrap.view {
	templateRootPath = EXT:pxa_bootstrap/Resources/Private/Templates/
	partialRootPath = EXT:pxa_bootstrap/Resources/Private/Partials/
	layoutRootPath = EXT:pxa_bootstrap/Resources/Private/Layouts/
}

plugin.tx_pxabootstrap {
	settings {
		layout {
			useLayout = {$tx_pxabootstrap.config.layout.useLayout}
			useHeader = {$tx_pxabootstrap.config.layout.useHeader}
			useFooter = {$tx_pxabootstrap.config.layout.useFooter}
		}

		less {
			#Debug : > 0 = enabled, 2 more extensive, also tries to parese each less file.
			debug = {$tx_pxabootstrap.config.displayDebugData}
			compress = {$tx_pxabootstrap.config.compress}
			outputCssFilename = {$tx_pxabootstrap.config.outputCssFilename}
			disabled = {$tx_pxabootstrap.config.disableLessCompilation}

				# If enabled, we dont't try to determine the relative path to the files but instead always add prefix to files in css.
			cssUrlPrefix {
				enabled = {$tx_pxabootstrap.config.cssUrlPrefixEnable}
				prefix = {$tx_pxabootstrap.config.cssUrlPrefix}
			}

			includes {
				variables {
						# Try to only place less files here with variables and mixins
						# The "section" variables is always included when trying to parse less files in debug mode.
						# Less files in other "sections" are expected to output some css after parsing it together with variables and mixins.
						# Do not add comments in variable files, then it will be outputted when parsing less files and won't "detect" empty files.
					10 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/variables.less
					20 = EXT:pxa_bootstrap/Resources/Public/Css/variables.less
				}
				mixins {
					// Utilities
					100 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/hide-text.less
					110= EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/opacity.less
					120 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/image.less
					130 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/labels.less
					140 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/reset-filter.less
					150 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/resize.less
					160 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/responsive-visibility.less
					170 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/size.less
					180 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/tab-focus.less
					190 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/text-emphasis.less
					200 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/text-overflow.less
					210 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/vendor-prefixes.less

					// Components
					220 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/alerts.less
					230 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/buttons.less
					240 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/panels.less
					250 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/pagination.less
					260 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/list-group.less
					270 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/nav-divider.less
					280 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/forms.less
					290 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/progress-bar.less
					300 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/table-row.less

					// Skins
					310 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/background-variant.less
					320 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/border-radius.less
					330 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/gradients.less

					// Layout
					340 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/clearfix.less
					350 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/center-block.less
					360 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/nav-vertical-align.less
					370 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/grid-framework.less
					380 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/mixins/grid.less
				}
				utilityClasses {
					10 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/utilities.less
					20 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/responsive-utilities.less
				}
				coreCss {
					10 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/scaffolding.less
					20 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/type.less
					30 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/code.less
					40 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/grid.less
					50 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/tables.less
					60 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/forms.less
					70 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/buttons.less
				}
				reset {
					10 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/normalize.less
					20 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/print.less
				}
				components {
					// Components
					10 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/component-animations.less
					20 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/glyphicons.less
					30 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/dropdowns.less
					40 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/button-groups.less
					50 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/input-groups.less
					60 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/navs.less
					70 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/navbar.less
					80 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/breadcrumbs.less
					90 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/pagination.less
					100 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/pager.less
					110 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/labels.less
					120 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/badges.less
					130 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/jumbotron.less
					140 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/thumbnails.less
					150 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/alerts.less
					160 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/progress-bars.less
					170 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/media.less
					180 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/list-group.less
					190 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/panels.less
					200 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/wells.less
					210 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/close.less
				}
				componentsWithJs {
					10 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/modals.less
					20 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/tooltip.less
					30 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/popovers.less
					40 = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/less/carousel.less
				}
				t3content {
					10 = EXT:pxa_bootstrap/Resources/Public/Css/t3content.less
				}
			}
		}
	}
}
	# If less compilation is disabled, empty the less ts configuration and add css file for default content.
	# 	Note: The bootstrap font path is overriden in file
	# 	EXT:pxa_bootstrap/Resources/Public/Css/variables.less
	#	, and if needed will have to be included some other way.
[globalString = LIT:1 = {$tx_pxabootstrap.config.disableLessCompilation}]
		# Empty the less configuration for includes, not all since they are used anyway.
	plugin.tx_pxabootstrap.settings.less.includes >
		# Add a css that will style some TYPO3 content elements that has been overriden from default css_styled_content output
	page.includeCSS.t3contentBs3 = EXT:pxa_bootstrap/Resources/Public/Css/t3contentBs3.css

[global]

	# Check if js files should be outputted.
[globalString = LIT:1 = {$tx_pxabootstrap.config.includeBootstrapJsInFooterlibs}]
		# Include bootstrap js files
	page.includeJSFooterlibs {
		bootstrapTransition = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/js/transition.js
		bootstrapAlert = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/js/alert.js
		bootstrapButton = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/js/button.js
		bootstrapCarousel = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/js/carousel.js
		bootstrapCollapse = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/js/collapse.js
		bootstrapDropDown = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/js/dropdown.js
		bootstrapModal = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/js/modal.js
		bootstrapTooltip = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/js/tooltip.js
		bootstrapPopover = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/js/popover.js
		bootstrapScrollspy = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/js/scrollspy.js
		bootstrapTab = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/js/tab.js
		bootstrapAffix = EXT:pxa_bootstrap/Resources/Public/Contrib/bootstrap/js/affix.js
	}
[global]
