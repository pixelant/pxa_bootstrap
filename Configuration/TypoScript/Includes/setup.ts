<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_bootstrap/Configuration/TypoScript/Includes/Setup/config.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_bootstrap/Configuration/TypoScript/Includes/Setup/page.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_bootstrap/Configuration/TypoScript/Includes/Setup/styles.ts">

// Extensions
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_bootstrap/Configuration/TypoScript/Includes/Setup/Extensions/felogin.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_bootstrap/Configuration/TypoScript/Includes/Setup/Extensions/news.ts">
